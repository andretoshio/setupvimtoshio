set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
" Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub
" Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
" Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
" Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Avoid a name conflict with L9
" Plugin 'user/L9', {'name': 'newL9'}

"my plugins
Plugin 'vim-scripts/autoload_cscope.vim'
Plugin 'chazy/cscope_maps'
Plugin 'Valloric/YouCompleteMe'
Plugin 'vim-scripts/AutoTag'
Plugin 'embear/vim-localvimrc'
Plugin 'kien/ctrlp.vim'
Plugin 'mileszs/ack.vim'
Plugin 'vim-airline/vim-airline'
"Plugin 'vivien/vim-linux-coding-style'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line


"map para criacao das tags
"You have to build your database with at least the following options:
"    	--c++-kinds=+p  : Adds prototypes in the database for C/C++ files.
"    	--fields=+iaS   : Adds inheritance (i), access (a) and
"                           	function signatures (S) information.
"    	--extra=+q  	: Adds
"                           	context to the tag name.
"                           	Note: Without this option, the script cannot
"                                 	get class members.
map <C-F12> :!ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .<CR>
map <C-F11> :!cscope -Rb <CR>

"Procura o arquivo tags do diretorio corrente subindo ate o diretorio HOME
set tags=./tags,tags;$HOME

filetype plugin on

"autotag para apendar novas tags conforme forem criadas
source ~/.vim/bundle/AutoTag/plugin/autotag.vim

"procura pelo arquivo cscope.out
source ~/.vim/bundle/autoload_cscope.vim/plugin/autoload_cscope.vim

"habilita cscope
source ~/.vim/bundle/cscope_maps/plugin/cscope_maps.vim

"source localvim
source ~/.vim/bundle/vim-localvimrc/plugin/localvimrc.vim

"source ack
source ~/.vim/bundle/ack.vim/plugin/ack.vim

let g:ycm_global_ycm_extra_conf = "~/.vim/.ycm_extra_conf.py"
let g:ycm_key_list_select_completion=[]
let g:ycm_key_list_previous_completion=[]
let g:ycm_add_preview_to_completeopt=0


nnoremap <leader>y :let g:ycm_auto_trigger=0<CR>                " turn off YCM
nnoremap <leader>Y :let g:ycm_auto_trigger=1<CR>                "turn on YCM

let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'

set wildignore+=*/tmp/*,*.so,*.swp,*.zip     
let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn)$'
let g:ctrlp_user_command = 'find %s -type f'
let g:ycm_register_as_syntastic_checker=0

let g:localvimrc_sandbox = 0
let g:localvimrc_name = ".lvimrc"

set tabstop=4
set shiftwidth=4
set softtabstop=4

set autoindent
set cindent

map _F mz[[k"xyy`z:echo @x<CR>
set hlsearch

set clipboard=unnamedplus

let g:ycm_auto_trigger=0
let g:loaded_youcompleteme = 0
let g:ycm_register_as_syntastic_checker = 0
set conceallevel=2

set relativenumber
set number

