#!/bin/bash

check_system()
{
    if [ $UID == 0 ]; then
        exit_error "You shouldn't run this script as root!"
    fi
}

main()
{
    echo "Checking system..."
    check_system

	if [ ! -d ~/.vim/bundle/Vundle.vim ]; then
		echo "Cloning Vundle..."
		git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim
	fi    

    echo "Installing..."
	if [ ! -f ~/.vim/.ycm_extra_conf.py ]; then
		cp ycm_extra_conf.py ~/.vim/.ycm_extra_conf.py
	fi

	if [ -f ~/.vimrc ]; then
		mv ~/.vimrc ~/.vimrc_backup	
	fi
	cp vimrc ~/.vimrc	

	echo "Installing plugins"
	vim +PluginInstall +qall

	echo "Compiling ycm"
	~/.vim/bundle/YouCompleteMe/install.sh --clang-completer

	echo "Done"
}

main

